require.config({
    baseUrl: "src",
    paths: {
        app: "app",
        jquery: "../libs/jquery/jquery-2.1.3.min",
        underscore: "../libs/lodash/lodash-2.4.1.min",
        backbone: "../libs/backbone/backbone.min",
        text: "../libs/requirejs-plugins/text",
        'bootstrap': "../libs/bootstrap/js/bootstrap.min",
        "highcharts": "../libs/highcharts/highcharts.min",
        "highcharts-more": "../libs/highcharts/highcharts-more.min"
    },
    shim: {
        "app": {
            deps: ["jquery", "underscore", "backbone", "highcharts", "highcharts-more"],
            exports: "app"
        },
        "backbone": {
            deps: ["jquery", "underscore"],
            exports: "Backbone"
        },
        "underscore": {
            exports: "_"
        },
        "boostrap": {
            deps: ["jquery"]
        },
        "highcharts": {
            deps: ["jquery"]
        },
        "highcharts-more": {
            deps: ["jquery", "highcharts"]
        }
    },
    /*hbs plugin options*/
    hbs: {
        disableI18n: true,
        templateExtension: "html"
    },
    waitSeconds: 60
});
require(["app"],

    function (App) {
        new App;
    }
);