define([
        "views/Header",
        "views/Calendar",
        "views/Stats",
        "models/TaskModel"
    ],
    function (Header, Calendar, Stats, TaskModel) {
        "use strict";

        var init = false;

        var $body;
        var $document;
        var $nashContainer;
        var $nashHeader;
        var $nashBody;

        var headerView;
        var calendarView;
        var statsView;

        var CALENDAR_TYPE_ENUM = {
            "month": "MONTH",
            "day": "DAY"
        };
        var CURRENT_VIEW_TYPE_ENUM = {
            "calendar": "CALENDAR",
            "stats": "STATS"
        };

        var currentViewType = null;
        var currentCalendarType = null;

        var currentDate;
        var taskCollection;

        var TASKS_COLLECTION_LOCAL_STORAGE_KEY = "nash-taskCollections";

        var taskRunningInterval;
        var currentRunningTask;

        return {
            init: function () {
                if (init) {
                    return;
                }

                init = true;

                $body = $("body");
                $document = $(document);
                $nashContainer = $("#nash-container");
                $nashHeader = $("#nash-header");
                $nashBody = $("#nash-body");

                currentDate = new Date();

                taskCollection = this._getCollectionFromLocalStorageIfExists();
                this._saveCollectionToLocalStorage();

                currentCalendarType = CALENDAR_TYPE_ENUM.month;
                currentViewType = CURRENT_VIEW_TYPE_ENUM.calendar;

                headerView = new Header({
                    calendarType: currentCalendarType,
                    currentDate: currentDate,
                    viewType: currentViewType,
                    manager: this
                });

                $nashHeader.append(headerView.render().$el);

                if(currentViewType === CURRENT_VIEW_TYPE_ENUM.stats){
                    this.switchToStatsView();
                }
                else{
                    this.switchToCalendarView();
                }

            },

            switchToStatsView: function () {
                currentViewType = CURRENT_VIEW_TYPE_ENUM.stats;
                if (!statsView) {
                    statsView = new Stats({
                        manager: this,
                        calendarType: currentCalendarType,
                        currentDate: currentDate
                    });
                }
                statsView.setCalendarType(currentCalendarType);
                statsView.setCurrentDate(currentDate);
                $nashBody.html(statsView.render().$el);
            },
            switchToCalendarView: function () {
                currentViewType = CURRENT_VIEW_TYPE_ENUM.calendar;
                if (!calendarView) {
                    calendarView = new Calendar({
                        manager: this,
                        calendarType: currentCalendarType,
                        currentDate: currentDate
                    });
                }
                calendarView.setCalendarType(currentCalendarType);
                calendarView.setCurrentDate(currentDate);
                $nashBody.html(calendarView.render().$el);
            },
            switchToDay: function () {
                currentCalendarType = CALENDAR_TYPE_ENUM.day;
                if (currentViewType === CURRENT_VIEW_TYPE_ENUM.calendar) {
                    calendarView.switchCalendarType(currentCalendarType);
                }
                else {
                    statsView.switchCalendarType(currentCalendarType);
                }

            },
            switchToMonth: function () {
                currentCalendarType = CALENDAR_TYPE_ENUM.month;
                if (currentViewType === CURRENT_VIEW_TYPE_ENUM.calendar) {
                    calendarView.switchCalendarType(currentCalendarType);
                }
                else {
                    statsView.switchCalendarType(currentCalendarType);
                }
            },

            previousDay: function () {
                currentDate.setDate(currentDate.getDate() - 1);
                this._handleDateChanged();
            },
            nextDay: function () {
                currentDate.setDate(currentDate.getDate() + 1);
                this._handleDateChanged();
            },
            previousMonth: function () {
                currentDate.setMonth(currentDate.getMonth() - 1);
                this._handleDateChanged();
            },
            nextMonth: function () {
                currentDate.setMonth(currentDate.getMonth() + 1);
                this._handleDateChanged();
            },
            _handleDateChanged: function () {
                if (currentViewType === CURRENT_VIEW_TYPE_ENUM.calendar) {
                    calendarView.changeCurrentDate(currentDate);
                }
                else {
                    statsView.changeCurrentDate(currentDate);
                }
            },

            getCurrentDate: function () {
                return currentDate;
            },
            setCurrentDate: function (date) {
                currentDate = date;
            },
            handleDateChangedFromCalendar: function () {
                headerView.reRenderWithNewDateAndType(currentDate, currentCalendarType);
            },

            addTaskToCollection: function (taskModel) {
                taskCollection.add(taskModel);
                this._saveCollectionToLocalStorage();
                this._handleNewTaskAdded(taskModel);
            },
            removeTaskFromCollection: function (taskModel) {

            },

            _saveCollectionToLocalStorage: function () {
                localStorage.setItem(TASKS_COLLECTION_LOCAL_STORAGE_KEY,
                    JSON.stringify(
                        taskCollection.toJSON()
                    )
                );
            },
            _getCollectionFromLocalStorageIfExists: function () {
                var ls_collection;
                var collectionFromLocalStorage;
                try {
                    ls_collection = localStorage.getItem(TASKS_COLLECTION_LOCAL_STORAGE_KEY);
                    if (ls_collection) {
                        ls_collection = JSON.parse(ls_collection);
                    }

                    collectionFromLocalStorage = new Backbone.Collection();
                    ls_collection.forEach(function (jsonModel) {
                        collectionFromLocalStorage.add(new TaskModel(jsonModel));
                    }, this);

                    if (this._isCollectionFromLocalStorageValid(collectionFromLocalStorage)) {
                        taskCollection = collectionFromLocalStorage;
                    }
                    else {
                        taskCollection = new Backbone.Collection();
                    }
                } catch (e) {
                    taskCollection = new Backbone.Collection();
                }
                return taskCollection;
            },

            getMonthlyTasks: function (year, month) {
                return new Backbone.Collection(
                    taskCollection.where({"nashMonth": year + "-" + month})
                );
            },
            getDailyTasks: function (year, month, day) {
                return new Backbone.Collection(
                    taskCollection.where({"nashDate": year + "-" + month + "-" + day})
                );
            },

            _isCollectionFromLocalStorageValid: function (tasks) {
                var i = 0;
                var taskModel;

                var now = new Date();
                var taskNashDate;

                if (tasks.length) {
                    for (i; i < tasks.models.length; i++) {
                        taskModel = tasks.models[i];
                        taskNashDate = taskModel.get("nashDate");
                        if (!taskNashDate) { //super basic validation
                            return false;
                        }
                    }
                }
                return true;
            },

            _handleNewTaskAdded: function (taskModel) {
                if (currentViewType === CURRENT_VIEW_TYPE_ENUM.calendar) {
                    calendarView.handleTasksUpdated(taskModel);
                }
                else {
                    // statsView.changeCurrentDate(currentDate);
                }
            },

            startTask: function(taskId){
                // pause current running task
                if(currentRunningTask){
                    this.pauseTask(currentRunningTask.get("uid"));
                }
                var taskModel = this._getTaskModelById(taskId);
                if(taskModel){
                    currentRunningTask = taskModel;
                    taskRunningInterval = setInterval(this._handleRunningTask.bind(this), 1000);
                }
            },
            _handleRunningTask: function(){
                if(!currentRunningTask){
                    return;
                }
                currentRunningTask.set("duration", currentRunningTask.get("duration") + 1);
                if (currentViewType === CURRENT_VIEW_TYPE_ENUM.calendar &&
                    currentCalendarType === CALENDAR_TYPE_ENUM.day) {
                    calendarView.handleTaskDurationChanged(currentRunningTask.get("uid"), currentRunningTask.get("duration"));
                    this._saveCollectionToLocalStorage();
                }
            },

            pauseTask: function(taskId){
                currentRunningTask = null;
                clearInterval(taskRunningInterval);
            },
            completeTask: function(taskId){
                currentRunningTask.set("completed", true);
                currentRunningTask = null;
                clearInterval(taskRunningInterval);
                if (currentViewType === CURRENT_VIEW_TYPE_ENUM.calendar &&
                    currentCalendarType === CALENDAR_TYPE_ENUM.day) {
                    calendarView.handleTaskStatusChanged(taskId);
                }
            },
            _getTaskModelById: function(taskId){
                return taskCollection.findWhere({
                    "uid": taskId
                });
            }

        }

    });