define(["nash"],
    function (nash) {
        "use strict";

        return Backbone.Router.extend({

            routes: {
                '': 'home'
            },


            initialize: function () {},

            home: function () {
                nash.init();
            },

            _changeTitle: function(title){
                this.$document.attr('title', title);
            },

            startHistory: function () {
                Backbone.history.start({
                    //pushState: true,
                    //root: ""
                });
            }

        });


    });