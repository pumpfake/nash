define([], function () {
    "use strict";

    function generateUUID() {
        var d = new Date().getTime();
        var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = (d + Math.random() * 16) % 16 | 0;
            d = Math.floor(d / 16);
            return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
        });
        return uuid;
    }

    /*
     uid:
     type: type,
     name: name,
     createDate: new Date(),
     date: this._currentDate,
     completed: false,
     duration: 0 // in seconds,
     obsolete: false,
     nashDate:
     nashMonth:
     */

    return Backbone.Model.extend({
        initialize: function (options) {

            var now = new Date();
            var taskDate;

            if(typeof options.date === "string"){
                options.date = new Date(options.date);
            }

            // we're only interested the day
            if (!options.nashDate) {
                this.set("nashDate",
                    options.date.getYear() + "-" + options.date.getMonth() + "-" + options.date.getDate()
                );
            }
            if (!options.nashMonth) {
                this.set("nashMonth",
                    options.date.getYear() + "-" + options.date.getMonth()
                );
            }

            if (!options.time_logs) {
                this.set("time_logs", []);
            }
            if (options.completed === null || options.completed === undefined) {
                this.set("completed", false);
            }
            if (options.obsolete === null || options.obsolete === undefined) {
                this.set("obsolete", false);
            }
            if (options.duration === null || options.duration === undefined) {
                this.set("duration", 0);
            }

            if (!options.uid) {
                this.set("uid", generateUUID());
            }

            taskDate = this.get("date");
            if(typeof taskDate === "string"){
                taskDate = new Date(taskDate);
            }
            taskDate.setHours(23, 59, 59);

            // user can no longer do this task
            if (( !this.get("completed") ) && taskDate < now) {
                this.set("obsolete", true);
            }

        },
        getTaskDuration: function () {
            return true;
        }

    });
});