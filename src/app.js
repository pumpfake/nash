define(["Router"],
    function(Router) {
        "use strict";

        return function(){
            var router;
            router = new Router();
            router.startHistory();
        };

    });