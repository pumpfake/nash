define(["text!templates/calendar/month.html",
        "text!templates/calendar/dayInMonth.html",
        "text!templates/calendar/taskOfDayInMonth.html",
        "enums/enums"
    ],
    function (monthTmpl, dayInMonthTmpl, taskOfDayInMonthTmpl, enums) {
        "use strict";

        return Backbone.View.extend({
            className: "nash-calendar-month",

            events: {
                //"click td.nash-dayInMonth-future": "_handleSingleClick",
                "click .nash-dayInMonth-addTaskToDate": "_promptToAddTask",
                "dblclick td.nash-dayInMonth-thisMonth": "_goToDayView"
            },

            initialize: function (options) {
                this._calendar = options.calendar;
                this._currentDate = options.currentDate;
            },
            render: function () {
                this.undelegateEvents();
                this.$el.remove();
                this.$el.html(_.template(monthTmpl));
                this._monthlyTasksCollection = this._calendar.getMonthlyTasks(this._currentDate.getYear(), this._currentDate.getMonth());
                this._renderCalendarDays();
                this.delegateEvents();
                return this;
            },
            _renderCalendarDays: function () {
                var $tbody = this.$("#nash-calendar-daysinmonth");

                $tbody.html();

                var firstDayOfMonth = new Date(this._currentDate.getFullYear(), this._currentDate.getMonth(), 1);
                var lastDayOfMonth = new Date(this._currentDate.getFullYear(), this._currentDate.getMonth() + 1, 0);

                var dayInMonthOfFirstDayOfMonth = firstDayOfMonth.getDate();
                var dayInMonthOfLastDayOfMonth = lastDayOfMonth.getDate();
                var totalNumberOfDaysInMonth = dayInMonthOfLastDayOfMonth;

                var dayInWeekOfFirstDayOfMonth = firstDayOfMonth.getDay();

                var rowCounter;
                var dayInWeekCounter;
                var dayInMonthCounter = 0;

                var $weekRow = $("<tr>");
                var $dayCol;

                var isCurrentInThisMonth;
                var isToday;
                var isPast;

                var todayDate = new Date();
                var isSameMonthWithToday = this._currentDate.getYear() === todayDate.getYear() &&
                    this._currentDate.getMonth() === todayDate.getMonth();
                var isOlderMonthThanToday = firstDayOfMonth < todayDate && !isSameMonthWithToday;
                var isNewerMonthThanToday = lastDayOfMonth > todayDate && !isSameMonthWithToday;

                var todayDateInMonthIndex = todayDate.getDate() - 1;

                // render first week
                for (dayInWeekCounter = 0; dayInWeekCounter < 7; dayInWeekCounter++) {
                    isCurrentInThisMonth = dayInWeekCounter >= dayInWeekOfFirstDayOfMonth;
                    isPast = isOlderMonthThanToday || (isSameMonthWithToday && dayInMonthCounter < todayDateInMonthIndex);
                    $dayCol = this._getDayOfMonthTh(dayInMonthCounter + 1, !isCurrentInThisMonth, isPast);
                    isToday = isSameMonthWithToday && (dayInMonthCounter === todayDateInMonthIndex);
                    if (isToday) {
                        $dayCol.addClass("nash-dayInMonth-today");
                    }
                    if (isCurrentInThisMonth) {
                        dayInMonthCounter++;
                    }
                    $weekRow.append($dayCol);
                }
                $tbody.append($weekRow);

                // render the next 5 weeks
                for (rowCounter = 0; rowCounter < 5; rowCounter++) {
                    $weekRow = $("<tr>");
                    for (dayInWeekCounter = 0; dayInWeekCounter < 7; dayInWeekCounter++) {
                        isCurrentInThisMonth = dayInMonthCounter < totalNumberOfDaysInMonth;
                        isPast = isOlderMonthThanToday || (isSameMonthWithToday && (dayInMonthCounter < todayDateInMonthIndex));
                        $dayCol = this._getDayOfMonthTh(dayInMonthCounter + 1, !isCurrentInThisMonth, isPast);
                        isToday = isSameMonthWithToday && (dayInMonthCounter === todayDateInMonthIndex);
                        if (isToday) {
                            $dayCol.addClass("nash-dayInMonth-today");
                        }
                        if (isCurrentInThisMonth) {
                            dayInMonthCounter++;
                        }
                        $weekRow.append($dayCol);
                    }
                    $tbody.append($weekRow);
                }

            },
            _getTasksOfDayInMonthHTML: function (dayInMonth) {
                var tasks = this._monthlyTasksCollection.where({
                    "nashDate": this._currentDate.getYear() + "-" + this._currentDate.getMonth() + "-" + dayInMonth
                });
                var taskElms = "";
                tasks.forEach(function (task) {
                    taskElms += _.template(taskOfDayInMonthTmpl, {
                        taskClass: enums.taskCssClass[task.get("type")],
                        iconClass: enums.taskCssIconClass[task.get("type")],
                        completedClass: task.get("completed") ? "taskCompleted" : ""
                    });
                });
                return taskElms;
            },

            _getDayOfMonthTh: function (currentDay, isOtherMonth, isPast) {
                var dayOfMonthTh;
                if (isOtherMonth) {
                    dayOfMonthTh = _.template(dayInMonthTmpl, {
                        date: "",
                        cssClass: "nash-dayInMonth-otherMonth",
                        tasks: ""
                    });
                }
                else {
                    dayOfMonthTh = (_.template(dayInMonthTmpl, {
                        date: currentDay,
                        cssClass: isPast ? "nash-dayInMonth-thisMonth nash-dayInMonth-past" : "nash-dayInMonth-thisMonth nash-dayInMonth-future",
                        tasks: this._getTasksOfDayInMonthHTML(currentDay)
                    }));
                }
                return $(dayOfMonthTh);
            },

            // obsolete for now
            _handleSingleClick: function (e) {
                var self = this;
                var $clickedElm = $(e.target).closest("td.nash-dayInMonth");
                var dblClickedValue = parseInt($clickedElm.data('double'), 10) || 0;
                $clickedElm.data('double', dblClickedValue + 1);
                setTimeout(function () {
                    var dblClickedValue = parseInt($clickedElm.data('double'), 10);
                    var dayInMonthValue;
                    if (dblClickedValue <= 1) {
                        dayInMonthValue = parseInt($clickedElm.data("dayinmonth"), 10);
                        self._promptToAddTask.call(self, dayInMonthValue);
                        $clickedElm.data('double', 0);
                    }
                }, 300);
            },
            _promptToAddTask: function (e) {
                var $clickedElm = $(e.target).closest("td.nash-dayInMonth");
                var dayInMonthValue = parseInt($clickedElm.data("dayinmonth"), 10);
                if (this._calendar.getCalendarType() !== "MONTH") {
                    return;
                }
                this._currentDate.setDate(dayInMonthValue);
                this._calendar.addTaskForSpecificDate(this._currentDate);
            },

            _goToDayView: function (e) {
                var $clickedElm = $(e.target).closest("td.nash-dayInMonth");
                var dayInMonthValue = parseInt($clickedElm.data("dayinmonth"), 10);
                setTimeout(function () {
                    $clickedElm.data('double', 0); // do the clean-up
                }, 300);
                this._currentDate.setDate(dayInMonthValue);
                this._calendar.goToDayViewOfSpecificDate(this._currentDate);
            },

            handleNewTaskAdded: function (year, month, date) {
                var tasksHTML;
                var $days;
                var $day;
                var i = 0;
                this._monthlyTasksCollection = this._calendar.getMonthlyTasks(this._currentDate.getYear(), this._currentDate.getMonth());
                if (year === this._currentDate.getYear() &&
                    month === this._currentDate.getMonth()) {
                    $days = this.$(".nash-dayInMonth");
                    for (; i < $days.length; i++) {
                        $day = $($days[i]);
                        if (parseInt($day.data("dayinmonth"), 10) === date) {
                            tasksHTML = this._getTasksOfDayInMonthHTML(date);
                            $day.find(".nash-dayInMonth-tasks").html(tasksHTML);
                            break;
                        }
                    }
                }
            }

        });
    });