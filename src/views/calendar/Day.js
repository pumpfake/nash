define(["text!templates/calendar/day.html",
        "text!templates/calendar/taskOfDay.html",
        "text!templates/calendar/taskOfTodayFunc.html",
        "enums/enums"],
    function (dayTmpl, taskOfDayTmpl, taskOfTodayFuncTmpl, enums) {
        "use strict";

        var TASK_MAX_SIZE = 280;
        var TASK_MIN_SIZE = 160;
        var TASK_SIZE_PER_HOUR = (TASK_MAX_SIZE - TASK_MIN_SIZE) / 12; // we assume one task wont be longer than 12 hours

        function pad(num, size) {
            var s = num+"";
            while (s.length < size) s = "0" + s;
            return s;
        }

        return Backbone.View.extend({
            className: "nash-calendar-day container",

            events: {
                "click #nash-addTaskToDate-btn": "_promptToAddTask",
                "click .nash-taskFunc-startTask": "_startTask",
                "click .nash-taskFunc-pauseTask": "_pauseTask",
                "click .nash-taskFunc-completeTask": "_completeTask"
            },

            initialize: function (options) {
                this._calendar = options.calendar;
            },
            render: function () {
                this.undelegateEvents();
                //this.$el.remove();
                this.$el.html(_.template(dayTmpl, {
                    date: this._calendar.getCurrentDate()
                }));

                if(!(this._isToday() || this._isFuture())){
                    this.$("#nash-addTaskToDate-btn").remove(); //cannot add tasks for past days
                }

                this._renderTasks();
                this.delegateEvents();
                return this;
            },

            _isToday: function () {
                var now = new Date();
                var cd = this._calendar.getCurrentDate();
                return cd.getYear() === now.getYear() &&
                    cd.getMonth() === now.getMonth() &&
                    cd.getDate() === now.getDate();
            },

            _isFuture: function(){
                var now = new Date();
                return this._calendar.getCurrentDate() > now;
            },

            _renderTasks: function () {
                var cd = this._calendar.getCurrentDate();
                var taskCollection = this._calendar.getDailyTasks(cd.getYear(), cd.getMonth(), cd.getDate());
                var $taskContainer = this.$(".nash-calendar-day-tasks");
                $taskContainer.html("");
                setTimeout(function(){
                    $taskContainer.addClass("entered");
                },1);
                taskCollection.models.forEach(this._renderTask.bind(this, $taskContainer), this);
            },

            _renderTask: function($taskContainer, taskModel){
                $taskContainer.append(this._getTaskHTML(taskModel));
            },

            _getTaskHTML: function(taskModel){
                var statusText = "";
                if(taskModel.get("completed")){
                    statusText = "COMPLETED";
                }
                else if(taskModel.get("obsolete")){
                    statusText = "INCOMPLETE";
                }

                var duration = taskModel.get("duration");

                var $tpm = $(_.template(taskOfDayTmpl, {
                    uid: taskModel.get("uid"),
                    taskClass: enums.taskCssClass[taskModel.get("type")],
                    iconClass: enums.taskCssIconClass[taskModel.get("type")],
                    name: taskModel.get("name"),
                    status: statusText,
                    duration: this._getDisplayedDuration(duration),
                    taskCompletedClass: taskModel.get("completed") ? "taskCompleted" : ""
                }));

                var newSize;
                var additionalSize;

                if(duration){
                    additionalSize = (duration / 3600) * TASK_SIZE_PER_HOUR;
                    newSize = additionalSize + TASK_MIN_SIZE;
                    $tpm.css({
                        width: newSize,
                        height: newSize,
                        "borderRadius": newSize,
                        "padding": (newSize / 4) - 10
                    });
                }

                if(this._isToday() && !taskModel.get("completed")){
                    $tpm.append(_.template(taskOfTodayFuncTmpl));
                }

                return $tpm;
            },

            _promptToAddTask: function () {
                // manager date is always current date
                this._calendar.addTaskForSpecificDate(this._calendar.getCurrentDate());
            },

            _getDisplayedDuration: function(duration){
                var hour = Math.floor(duration / 3600);
                var minutes = Math.floor((duration - (hour * 3600)) / 60);
                var seconds = (duration - (hour * 3600 + minutes * 60));

                return pad(hour, 2) + ":" + pad(minutes, 2) + ":" + pad(seconds, 2);
            },

            handleNewTaskAdded: function (year, month, day) {
                this.render();
            },

            _startTask: function(e){
                $(".nash-calendar-day-task").removeClass("running");
                var $elm = $(e.target).closest(".nash-calendar-day-task");
                var taskId = $elm.data("taskid");
                this._calendar.startTask(taskId);
                $elm.addClass("running");
            },

            _pauseTask: function(e){
                $(".nash-calendar-day-task").removeClass("running");
                var $elm = $(e.target).closest(".nash-calendar-day-task");
                var taskId = $elm.data("taskid");
                this._calendar.pauseTask(taskId);
                $elm.removeClass("running");

            },
            _completeTask: function(e){
                $(".nash-calendar-day-task").removeClass("running");
                var $elm = $(e.target).closest(".nash-calendar-day-task");
                var taskId = $elm.data("taskid");
                this._calendar.completeTask(taskId);
                $elm.removeClass("running");
            },

            handleTaskStatusChanged: function(taskId, taskModel){
                this.render();
            },

            handleTaskDurationChanged: function(taskId, duration){
                var $elm = this._getTaskElmByTaskId(taskId);
                var newSize;
                var additionalSize;
                if ($elm) {
                    additionalSize = (duration / 3600) * TASK_SIZE_PER_HOUR;
                    newSize = additionalSize + TASK_MIN_SIZE;
                    $elm.css({
                        width: newSize,
                        height: newSize,
                        "borderRadius": newSize,
                        "padding": (newSize / 4) - 10
                    });
                    $elm.find(".nash-calendar-day-task-duration").html(this._getDisplayedDuration(duration));
                }
            },

            _getTaskElmByTaskId: function(taskId){
                var $tasks = this.$(".nash-calendar-day-task");
                var $task;
                var i = 0;

                for(; i < $tasks.length; i++){
                    $task = $($tasks[i]);
                    if($task.data("taskid") === taskId){
                        return $task;
                    }
                }
                return false;
            }


        });
    });