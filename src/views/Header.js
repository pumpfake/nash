define(["text!templates/header.html"],
    function (headerTmpl) {
        "use strict";

        var DISPLAYED_MONTH_LONG = ['January', 'February', 'March', 'April',
            'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        var DISPLAYED_MONTH_SHORT = ['Jan', 'Feb', 'March', 'April', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];

        var ICON_STATS_VIEW = "fa-line-chart";
        var ICON_CALENDAR_VIEW = "fa-calendar";

        return Backbone.View.extend({

            className: "nash-header-content",

            events: {
                "click #nash-header-switchView-btn": "_switchViewType",
                "click #nash-switchCalendarType-day-button": "_switchCalendarTypeToDay",
                "click #nash-switchCalendarType-month-button": "_switchCalendarTypeToMonth",
                "click #nash-calendar-prevNav": "_prevDate",
                "click #nash-calendar-nextNav": "_nextDate"
            },

            initialize: function (options) {
                this._currentDate = options.currentDate;
                this._calendarType = options.calendarType;
                this._viewType = options.viewType;
                this._manager = options.manager;
            },
            render: function () {
                this.$el.html(_.template(headerTmpl));

                this._handleDateChanged();
                this._handleViewTypeIcon();
                this._handleCalendarTypeIcon();
                return this;
            },

            _getDateDisplay: function () {
                var dateDisplay;

                var year = "<span class=\"nash-header-datedisplay-year\">"+
                    this._getCurrentDate().getFullYear() + "</span>";
                var month = DISPLAYED_MONTH_LONG[this._getCurrentDate().getMonth()];
                var date = this._getCurrentDate().getDate();

                switch (this._getCalendarType()) {
                    case "DAY":
                        dateDisplay = date + " " + month + " " + year;
                        break;
                    case "MONTH":
                    default:
                        dateDisplay = month + " " + year;
                        break;
                }

                return dateDisplay;

            },

            _getCalendarType: function () {
                return this._calendarType;
            },
            _setCalendarType: function(calendarType){
                this._calendarType = calendarType;
            },
            _getCurrentDate: function () {
                return this._currentDate;
            },
            _setCurrentDate: function(currentDate){
                this._currentDate = currentDate;
            },
            _getViewType: function(){
                return this._viewType;
            },
            _setViewType: function(viewType){
                this._viewType = viewType;
            },

            _switchViewType: function(){
                if(this._getViewType() === "CALENDAR"){
                    this._setViewType("STATS");
                    this._manager.switchToStatsView();
                }
                else{
                    this._setViewType("CALENDAR");
                    this._manager.switchToCalendarView();
                }
                this._handleViewTypeIcon();
            },
            _handleViewTypeIcon: function(){
                var isCalendar = this._getViewType() === "CALENDAR";
                var removeClass = isCalendar ? ICON_CALENDAR_VIEW : ICON_STATS_VIEW;
                var addClass = isCalendar ? ICON_STATS_VIEW : ICON_CALENDAR_VIEW;

                this.$("#nash-header-switchView-btn").removeClass(removeClass).addClass(addClass);
            },

            _switchCalendarTypeToDay: function(e){
                if($(e.target).hasClass("active")){
                    return;
                }
                this._setCalendarType("DAY");
                this._manager.switchToDay();
                this._handleCalendarTypeIcon();
                this._handleDateChanged();
            },
            _switchCalendarTypeToMonth: function(e){
                if($(e.target).hasClass("active")){
                    return;
                }
                this._setCalendarType("MONTH");
                this._manager.switchToMonth();
                this._handleCalendarTypeIcon();
                this._handleDateChanged();
            },
            _handleCalendarTypeIcon: function(){
                var isDay = this._getCalendarType() === "DAY";
                var activeButton = isDay ? "nash-switchCalendarType-day-button" : "nash-switchCalendarType-month-button";
                var inactiveButton = isDay ? "nash-switchCalendarType-month-button" : "nash-switchCalendarType-day-button";
                this.$("#" + activeButton).addClass("active");
                this.$("#" + inactiveButton).removeClass("active");
            },
            _handleDateChanged: function(){
                var dateDisplay = this._getDateDisplay();
                this.$(".nash-header-datedisplay").html(dateDisplay);
            },

            _prevDate: function(){
                if(this._getCalendarType() === "MONTH"){
                    this._manager.previousMonth();
                }
                else{
                    this._manager.previousDay();
                }

                this._setCurrentDate(this._manager.getCurrentDate());
                this._handleDateChanged();

            },
            _nextDate: function(){
                if(this._getCalendarType() === "MONTH"){
                    this._manager.nextMonth();
                }
                else{
                    this._manager.nextDay();
                }
                this._setCurrentDate(this._manager.getCurrentDate());
                this._handleDateChanged();
            },

            reRenderWithNewDateAndType: function(date, type){
                this._setCurrentDate(date);
                this._setCalendarType(type);
                this.render();
            }

        });

    });