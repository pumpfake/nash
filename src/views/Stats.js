define([
        "text!templates/stats/stats.html",
        "views/stats/Month",
        "views/stats/Day"
    ],
    function (statsTmpl, Month, Day) {
        "use strict";

        return Backbone.View.extend({

            className: "nash-stats-container",

            _monthView: null,
            _dayView: null,

            initialize: function (options) {
                this._currentDate = options.currentDate;
                this._manager = options.manager;
                this._calendarType = options.calendarType;
            },

            render: function () {
                var view;
                if (this._getCalendarType() === "MONTH") {
                    view = this._getMonthView();
                }
                else {
                    view = this._getDayView();
                }

                if (view) {
                    this.$el.html(view.render().$el);
                }
                return this;
            },

            _getMonthView: function () {
                if (this._monthView) {
                    return this._monthView;
                }
                this._monthView = new Month({
                    stats: this
                });
                return this._monthView;
            },
            _getDayView: function(){
                if (this._dayView) {
                    return this._dayView;
                }
                this._dayView = new Day({
                    stats: this
                });
                return this._dayView;
            },

            _getCalendarType: function () {
                return this._calendarType;
            },
            setCalendarType: function (calendarType) {
                this._calendarType = calendarType;
            },
            getCurrentDate: function () {
                return this._currentDate;
            },
            setCurrentDate: function (currentDate) {
                this._currentDate = currentDate;
            },

            switchCalendarType: function (calendarType) {
                this.setCalendarType(calendarType);
                this.render();
            },
            changeCurrentDate: function (currentDate) {
                this.setCurrentDate(currentDate);
                this.render();
            },

            getMonthlyTasks: function(year, month){
                return this._manager.getMonthlyTasks(year, month);
            },
            getDailyTasks: function(year, month, day){
                return this._manager.getDailyTasks(year, month, day);
            }

        });

    });