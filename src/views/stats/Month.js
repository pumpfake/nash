define([
        "text!templates/stats/month.html",
        "text!templates/stats/haveStatsMonth.html",
        "text!templates/stats/noStatsMonth.html",
        "enums/enums"
    ],
    function (monthTmpl, haveStatsMonthTmpl, noStatsMonthTmpl, enums) {
        "use strict";

        function getDisplayedDuration(duration) {
            var hour = Math.floor(duration / 3600);
            var minutes = Math.floor((duration - (hour * 3600)) / 60);
            var seconds = (duration - (hour * 3600 + minutes * 60));

            return pad(hour, 2) + ":" + pad(minutes, 2) + ":" + pad(seconds, 2);
        }

        function pad(num, size) {
            var s = num + "";
            while (s.length < size) s = "0" + s;
            return s;
        }

        var THIS_MONTH_MAIN_TEXT_TEMPLATE = "In this month, you have <b>${numOfTasks}</b> tasks, and you've completed <b>${numOfCompletedTasks}</b> of them so far.";

        return Backbone.View.extend({
            className: "nash-stats-month",

            events: {},

            initialize: function (options) {
                this._stats = options.stats;
            },
            render: function () {
                this.undelegateEvents();
                this.$el.remove();
                var cd = this._stats.getCurrentDate();
                this._monthlyTasksCollection = this._stats.getMonthlyTasks(
                    cd.getYear(), cd.getMonth()
                );
                this.$el.html(_.template(monthTmpl));

                var numOfTasks = this._monthlyTasksCollection.length;
                var numOfCompletedTasks = this._getNumberOfCompletedTasks();

                if (numOfTasks) {
                    this.$(".nash-stats-month-container").append(
                        _.template(haveStatsMonthTmpl, {
                            mainText: _.template(THIS_MONTH_MAIN_TEXT_TEMPLATE, {
                                numOfTasks: numOfTasks,
                                numOfCompletedTasks: (numOfCompletedTasks === 0) ? "none" :
                                    (numOfCompletedTasks === numOfTasks) ? "all" : numOfCompletedTasks
                            })
                        })
                    );
                    this._renderVisualisations();
                }
                else {
                    this.$(".nash-stats-month-container").append(
                        _.template(noStatsMonthTmpl)
                    );
                }

                this.delegateEvents();
                return this;
            },

            _getNumberOfCompletedTasks: function () {
                return this._monthlyTasksCollection.where({completed: true}).length;
            },

            _getNumberOfTasksByCategory: function (typeId) {
                return this._monthlyTasksCollection.where({type: typeId});
            },

            _renderVisualisations: function () {
                var self = this;
                setTimeout(function(){
                    self._renderDonutPie("nash-stats-month-chart-a");
                    self._renderBubbleChart("nash-stats-month-chart-b");
                }, 1);
            },

            _renderDonutPie: function (containerId) {
                var $container = this.$("#" + containerId);
                var taskCategories = [];
                var tasksData = [];

                enums.taskNames.forEach(function (taskCategoryName, index) {
                    var tasksByCategory = this._getNumberOfTasksByCategory(index);
                    var drillDataLen = tasksByCategory.length;
                    var brightness;

                    taskCategories.push({
                        name: taskCategoryName,
                        y: drillDataLen,
                        color: enums.taskColors[index]
                    });
                    tasksByCategory.forEach(function (task, taskIndex) {
                        brightness = 0.2 - (taskIndex / drillDataLen) / 5;
                        tasksData.push({
                            name: task.get("name"),
                            y: task.get("duration"),
                            color: Highcharts.Color(enums.taskColors[index]).brighten(brightness).get()
                        });
                    }, this);
                }, this);

                $container.highcharts({
                    title: null,
                    chart: {
                        type: 'pie'
                    },
                    tooltip: {
                        formatter: function () {
                            return this.point.name + '<br/>' +
                                'Total time Spent: ' + '<b>' + (getDisplayedDuration(this.y)) + "</b>";
                        }
                    },
                    credits: {
                        enabled: false
                    },
                    plotOptions: {
                        pie: {
                            shadow: false,
                            center: ['50%', '50%']
                        }
                    },
                    series: [{
                        name: 'Category tasks',
                        data: taskCategories,
                        size: '52%',
                        dataLabels: {
                            formatter: function () {
                                return this.y > 0 ? this.point.name : null;
                            },
                            color: 'white',
                            distance: -25
                        }
                    }, {
                        name: 'Time Spent',
                        data: tasksData,
                        size: '80%',
                        innerSize: '65%',
                        dataLabels: {
                            formatter: function () {
                                // display only if larger than 1
                                return this.y > 1 ? '<b>' + this.point.name + '</b>' :
                                    null;
                            },
                            distance: 10
                        }
                    }]
                });
            },

            _renderBubbleChart: function (containerId) {

                var data =
                    enums.taskNames.map(function (taskCategoryName, index) {
                        var tasksByCategory = this._getNumberOfTasksByCategory(index);
                        return tasksByCategory.map(function (task) {
                            var nashDate = task.get("nashDate");
                            var nashDateSplit = nashDate.split("-");
                            var taskCompleted = task.get("completed") ? 1 : 0;
                            return [(nashDateSplit[2] + 1), taskCompleted, task.get("duration")];  //day, completed, duration
                        });
                    }, this);

                this.$("#" + containerId).highcharts({
                    title: null,
                    chart: {
                        type: 'bubble'
                    },
                    tooltip: {
                        formatter: function () {
                            return "Day: " + "<b>" + this.x + "</b><br/>" +
                                "Completed: " + "<b>" + (this.y ? "Yes" : "No") + "</b><br/>" +
                                "Time Spent: " + "<b>" + getDisplayedDuration(this.point.name) + "</b>";

                        }
                    },
                    credits: {
                        enabled: false
                    },
                    series: [{
                        data: data[0],
                        color: enums.taskColors[0],
                        name: enums.taskNames[0]
                    }, {
                        data: data[1],
                        color: enums.taskColors[1],
                        name: enums.taskNames[1]
                    }, {
                        data: data[2],
                        color: enums.taskColors[2],
                        name: enums.taskNames[2]
                    }, {
                        data: data[3],
                        color: enums.taskColors[3],
                        name: enums.taskNames[3]
                    }
                    ]
                });
            }
        });
    });
