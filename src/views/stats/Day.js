define(["text!templates/stats/day.html",
        "text!templates/stats/haveStatsDay.html",
        "text!templates/stats/noStatsDay.html"],
    function (dayTmpl, haveStatsDay, noStatsDay) {
        "use strict";

        return Backbone.View.extend({
            className: "nash-stats-day",

            events: {},

            initialize: function (options) {
                this._stats = options.stats;
            },
            render: function () {
                this.undelegateEvents();
                this.$el.remove();
                var cd = this._stats.getCurrentDate();
                this._dailyTasksCollection = this._stats.getDailyTasks(
                    cd.getYear(), cd.getMonth(), cd.getDate()
                );
                this.$el.html(_.template(dayTmpl));

                var numOfTasks = this._dailyTasksCollection.length;
                var numOfCompletedTasks = this._getNumberOfCompletedTasks();

                var isToday = this._isToday();
                var isFuture = this._isToday();

                if (numOfTasks) {
                    this.$(".nash-stats-day-container").append(
                        _.template(haveStatsDay, {
                            "dayStart": isToday ? "Today" : "On this day",
                            "dayMiddle": isToday ? "have" : isFuture ? "have" : "had",
                            "numberOfTasks": numOfTasks
                        })
                    );
                }
                else {
                    this.$(".nash-stats-day-container").append(
                        _.template(noStatsDay)
                    );
                }

                this.delegateEvents();
                return this;
            },

            _isToday: function () {
                var now = new Date();
                var cd = this._stats.getCurrentDate();
                return cd.getYear() === now.getYear() &&
                    cd.getMonth() === now.getMonth() &&
                    cd.getDate() === now.getDate();
            },

            _isFuture: function(){
                var now = new Date();
                return this._stats.getCurrentDate() > now;
            },

            _getNumberOfCompletedTasks: function () {
                return this._dailyTasksCollection.where({completed: true}).length;
            },

            _getNumberOfTasksByCategory: function (typeId) {
                return this._dailyTasksCollection.where({type: typeId});
            }

        });
    });