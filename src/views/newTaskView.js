define(["bootstrap", "text!templates/newTask.html", "models/TaskModel"],
    function (bootstrap, newTaskTmpl, TaskModel) {
        "use strict";

        var TASK_NAME_PLACEHOLDER_STRINGS = [
            "eg. Meeting with Klay",
            "eg. Learn how webworkers work",
            "eg. C.O.D",
            "eg. Shoot jumpers with Steph"
        ];
        var TASK_DEFAULT_NAMES = [
            "Work",
            "Learn",
            "Play",
            "Exercise"
        ];

        var View = Backbone.View.extend({

            id: "nash-newTask-modal",
            className: "modal fade",
            taskTypeId: null,
            _manager: null,
            _currentDate: null,

            events: {
                "click .nash-taskType-selection": "_handleNewTaskTypeSelected",
                "click #nash-task-addBtn": "_addTask"
            },

            initialize: function(options){
                this._currentDate = options.currentDate;
                this._calendar = options.calendar;
                this.taskTypeId = options.taskTypeId;
            },
            render: function(){
                this.$el.html(_.template(newTaskTmpl, {}));
                this._renderSelectedTaskType();
                return this;
            },
            _handleNewTaskTypeSelected: function(e){
                var $elm = $(e.target).closest(".nash-taskType-selection");
                this.taskTypeId = parseInt($elm.data("tasktype"), 10) || 0;
                this._renderSelectedTaskType();
            },
            _renderSelectedTaskType: function(){
                var i = 0;
                var $selections = this.$(".nash-taskType-selection");
                var $selection;
                $selections.removeClass("selected");
                for(i; i < $selections.length; i++){
                    $selection = $($selections[i]);
                    if(parseInt($selection.data("tasktype"), 10) === this.taskTypeId){
                        $selection.addClass("selected");
                        this.$("#nash-taskNameInput").attr("placeholder", TASK_NAME_PLACEHOLDER_STRINGS[this.taskTypeId]);
                    }
                }
            },

            _addTask: function(){
                var type = this.taskTypeId;
                var taskNameInputVal = this.$("#nash-taskNameInput").val();
                var name = taskNameInputVal.length ? taskNameInputVal : TASK_DEFAULT_NAMES[type];

                var self = this;
                var tm = new TaskModel({
                    type: type,
                    name: name,
                    createDate: new Date(),
                    date: this._currentDate,
                    completed: false,
                    duration: 0
                });
                this._calendar.handleNewTaskAdded(this._currentDate, tm);

                setTimeout(function(){
                    self.$el.modal("hide");
                }, 10);
            }
        });

        return {

            createView: function (date, calendar) {
                var view = new View({
                    currentDate: date,
                    calendar: calendar,
                    taskTypeId: Math.floor(Math.random() * 3)
                });
                var $view = view.render().$el;
                $view.modal({
                    keyboard: true
                })
                    .on('hidden.bs.modal', function (e) {
                        this.remove();
                        view.remove();
                    });

                $("body").append($view);
                return view;
            }
        };

    });