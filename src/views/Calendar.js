define([
        "text!templates/calendar/calendar.html",
        "views/calendar/Month",
        "views/calendar/Day",
        "views/newTaskView"
    ],
    function (calendarTmpl, Month, Day, newTaskView) {
        "use strict";

        return Backbone.View.extend({

            className: "nash-calendar-container",

            _monthView: null,
            _dayView: null,
            _collection: null,

            initialize: function (options) {
                this._currentDate = options.currentDate;
                this._manager = options.manager;
                this._calendarType = options.calendarType;
            },

            render: function () {
                var view;
                if (this.getCalendarType() === "MONTH") {
                    view = this._getMonthView();
                }
                else {
                    view = this._getDayView();
                }

                if (view) {
                    this.$el.html(view.render().$el);
                }
                return this;
            },

            _getMonthView: function () {
                if (this._monthView) {
                    return this._monthView;
                }
                this._monthView = new Month({
                    calendar: this,
                    currentDate: this.getCurrentDate() // TODO: Make month view always use currentDate from here
                });
                return this._monthView;
            },
            _getDayView: function(){
                if (this._dayView) {
                    return this._dayView;
                }
                this._dayView = new Day({
                    calendar: this
                });
                return this._dayView;
            },

            getCalendarType: function () {
                return this._calendarType;
            },
            setCalendarType: function (calendarType) {
                this._calendarType = calendarType;
            },
            getCurrentDate: function () {
                return this._currentDate;
            },
            setCurrentDate: function(currentDate){
                this._currentDate = currentDate;
            },

            switchCalendarType: function (calendarType) {
                this.setCalendarType(calendarType);
                this.render();
            },
            changeCurrentDate: function(currentDate){
                this.setCurrentDate(currentDate);
                this.render();
            },

            goToDayViewOfSpecificDate: function(date){
                this._manager.setCurrentDate(date);
                this._manager.switchToDay();
                this._manager.handleDateChangedFromCalendar();
            },
            addTaskForSpecificDate: function(date){
                newTaskView.createView(date, this);
            },
            handleNewTaskAdded: function(date, taskModel){
                this._manager.addTaskToCollection(taskModel);
            },

            getDailyTasks: function(year, month, day){
                return this._manager.getDailyTasks(year, month, day);
            },
            getMonthlyTasks: function(year, month){
                return this._manager.getMonthlyTasks(year, month);
            },

            handleTasksUpdated: function(taskModel){
                var view;
                if (this.getCalendarType() === "MONTH") {
                    view = this._getMonthView();
                }
                else {
                    view = this._getDayView();
                }
                var nashDateSplit = taskModel.get("nashDate").split("-");
                //nashDateSplit = nashDateSplit.map(parseInt);
                nashDateSplit = nashDateSplit.map(function(d){
                    return parseInt(d, 10);
                });
                view.handleNewTaskAdded.apply(view, nashDateSplit);
            },

            startTask: function(taskId){
                this._manager.startTask(taskId);
            },
            pauseTask: function(taskId){
                this._manager.pauseTask(taskId);
            },
            completeTask: function(taskId){
                this._manager.completeTask(taskId);
            },

            handleTaskDurationChanged: function(taskId, duration){
                if (this.getCalendarType() === "DAY") {
                    this._getDayView().handleTaskDurationChanged(taskId, duration);
                }
            },

            handleTaskStatusChanged: function(taskId){
                if (this.getCalendarType() === "DAY") {
                    this._getDayView().handleTaskStatusChanged(taskId);
                }
            }

        });

    });