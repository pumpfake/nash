define([], function(){
    var TASK_CSS_CLASS = ["work", "learn", "play", "sport"];
    var TASK_CSS_ICON_CLASS = ["fa-suitcase", "fa-book", "fa-gamepad", "fa-bicycle"];
    var TASK_NAMES = [
        "Work",
        "Learn",
        "Play",
        "Sport"
    ];
    var TASK_COLORS = [
        "#44444F", "#66A8E8", "#EC4571", "#eca33b"
    ];

    return {
        taskCssClass: TASK_CSS_CLASS,
        taskCssIconClass: TASK_CSS_ICON_CLASS,
        taskNames: TASK_NAMES,
        taskColors: TASK_COLORS
    };
});