Replace this at line 188 of nash.js if u want to have some sampple data and don't want to use your localstorage data

                    var today = new Date();
                    var yesterday = new Date();
                    var tomorrow = new Date();
                    var someDay1 = new Date();
                    var someDay2 = new Date();
                    var someDay3 = new Date();

                    yesterday.setDate(today.getDate() - 1);
                    tomorrow.setDate(today.getDate() + 1);
                    someDay1.setDate(today.getDate() - 5);
                    someDay2.setDate(today.getDate() + 3);
                    someDay3.setDate(today.getDate() + 5);


ls_collection = [
                        /* someDay1 */
                        {
                            type: 0,
                            name: "Fix that stupid bug",
                            createDate: someDay1,
                            date: someDay1,
                            completed: true,
                            duration: 8.48 * 3600
                        },
                        {
                            type: 1,
                            name: "Javascript 101",
                            createDate: someDay1,
                            date: someDay1,
                            completed: true,
                            duration: 2.14 * 3600
                        },
                        {
                            type: 3,
                            name: "Train my abs",
                            createDate: someDay1,
                            date: someDay1,
                            completed: false,
                            duration: 0.155 * 3600
                        },

                        /* Yesterday */
                        {
                            type: 2,
                            name: "Play some chess",
                            createDate: yesterday,
                            date: yesterday,
                            completed: true,
                            duration: 1.48 * 3600
                        },
                        {
                            type: 3,
                            name: "Jog 2km",
                            createDate: yesterday,
                            date: yesterday,
                            completed: true,
                            duration: 0.45 * 3600
                        },
                        {
                            type: 3,
                            name: "Shoot 5000 hoops",
                            createDate: yesterday,
                            date: yesterday,
                            completed: false,
                            duration: 3.19 * 3600
                        },

                        /* Today */
                        {
                            type: 0,
                            name: "Brainstorming session",
                            createDate: today,
                            date: today,
                            completed: false,
                            duration: 0
                        },
                        {
                            type: 1,
                            name: "Learns how webworkers work",
                            createDate: today,
                            date: today,
                            completed: true,
                            duration: 4 * 3600
                        },
                        {
                            type: 2,
                            name: "NBA 2K15",
                            createDate: today,
                            date: today,
                            completed: false,
                            duration: 1.5 * 3600
                        },
                        {
                            type: 3,
                            name: "Hit the gym",
                            createDate: today,
                            date: today,
                            completed: true,
                            duration: 2.15 * 3600
                        },

                        /* someDay2 */
                        {
                            type: 2,
                            name: "Watch some youtube",
                            createDate: someDay2,
                            date: someDay2,
                            completed: false,
                            duration: 2.28 * 3600
                        },

                        /* someDay3 */
                        {
                            type: 3,
                            name: "Work on my handles",
                            createDate: someDay3,
                            date: someDay3,
                            completed: false,
                            duration: 4.28 * 3600
                        }

                    ];