This repo is created from the StaticShow's 2015 48-hour hackathon that was held between 25 Jan and 26 Jan 2015.

Please checkout:  
Contest entry page: http://2015.staticshowdown.com/teams/pumpfake  
Contest entry app: https://ss15-pumpfake.divshot.io  
About page: https://ss15-pumpfake.divshot.io/about  
Organizer: http://www.staticshowdown.com/  
  
  
We moved it from github to extend it's future possibilities.